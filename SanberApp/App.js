import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// import YoutubeUI from './Tugas/Tugas-12/App';
// import LoginScreen from './Tugas/Tugas-13/LoginScreen';
// import AboutScreen from './Tugas/Tugas-13/AboutScreen';
// import RegisterScreen from './Tugas/Tugas-13/RegisterScreen';
// import NoteApp from './Tugas/Tugas-14/App';
import Navigation from './Tugas/Tugas-15/TugasNavigation/index';

export default function App() {
  return (
    // <YoutubeUI />

    // <LoginScreen />

    // <RegisterScreen />

    // <AboutScreen />

    // <NoteApp />

    <Navigation />

    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
