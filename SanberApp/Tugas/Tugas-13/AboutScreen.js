import React from 'react';
import { Platform, StyleSheet, Text,  View,  Image,  TextInput,  TouchableOpacity,  Button,  KeyboardAvoidingView, ScrollView } from 'react-native';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

export default class App extends React.Component{
    render() {
        return(
            <ScrollView>
                <View style={styles.container}>
                    <Text style={styles.txtTentang}>Tentang Saya</Text>
                    <FontAwesome5 name='user-circle' size={200} style={styles.icon} color='#EFEFEF'/>
                    <Text style={styles.name}>Paijo Yoi</Text>
                    <Text style={styles.job}>Mahasiswa</Text>
                    <View style={styles.card}>
                        <Text style={styles.top}>Portofolio</Text>
                        <View style={styles.bot}>
                            <View>
                                <FontAwesome5 name='gitlab' size={36} color="#3EC6FF" style={styles.icon}/>
                                <Text style={styles.teks}>@Paijo</Text>
                            </View>  
                            <View>
                                <FontAwesome5 name='github' size={36} color="#3EC6FF" style={styles.icon}/>
                                <Text style={styles.teks}>@Paijo-m</Text>
                            </View>   
                        </View>
                    </View>

                    <View style={styles.card}>
                        <Text style={styles.top}>Hubungi Saya</Text>
                        <View style={styles.botContact}>
                            <View style={styles.botSubContact}>
                                <View>
                                    <FontAwesome5 name='facebook' size={36} color='#3EC6FF' style={styles.icon}/>
                                </View>
                                <View style={{justifyContent: 'center', marginLeft: 30}}>
                                    <Text style={styles.teks}>@paijo_iyoi</Text>
                                </View>
                            </View>
                            <View style={styles.botSubContact}>
                                <View>
                                    <FontAwesome5 name='instagram' size={36} color='#3EC6FF' style={styles.icon}/>
                                </View>
                                <View style={{justifyContent: 'center', marginLeft: 30}}>
                                    <Text style={styles.teks}>@paijo_iyoi</Text>
                                </View>
                            </View>
                            <View style={styles.botSubContact}>
                                <View>
                                    <FontAwesome5 name='twitter' size={36} color='#3EC6FF' style={styles.icon}/>
                                </View>
                                <View style={{justifyContent: 'center', marginLeft: 30}}>
                                    <Text style={styles.teks}>@paijo_iyoi</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 80,
        marginBottom: 40,
        flex: 1
    },
    txtTentang: {
        fontWeight: '700',
        fontSize: 36,
        textAlign: 'center',
        color: '#003366'
    },
    icon: {
        textAlign: 'center',
    },
    name: {
        color: '#003366',
        fontSize: 24,
        textAlign: 'center',
    },
    job: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#3EC6FF',
        textAlign: 'center',
        marginTop: 5
    },
    card: {
        borderRadius: 16,
        marginTop: 11,
        backgroundColor: '#EFEFEF',
        padding: 8,
        marginHorizontal: 10
    },
    top: {
        color: '#003366',
        fontSize: 18,
        borderBottomWidth: 1,
        borderBottomColor: '#003366',
        paddingBottom: 5
    },
    bot: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginVertical: 15
    },
    botContact: {
        marginTop: 15,
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    botSubContact: {
        flexDirection: 'row',
        justifyContent: 'center',
        height: 50
    },
    teks: {
        fontSize: 16,
        fontWeight: "bold",
        color: "#003366",
        textAlign: "center"
    }
});