import React from 'react';
import { Platform, StyleSheet, Text,  View,  Image,  TextInput,  TouchableOpacity,  Button,  KeyboardAvoidingView, ScrollView } from 'react-native';

export default class App extends React.Component{
    render() {
        return(
            <ScrollView>
                <View style={styles.containerView}>
                    <Image source={require('./assets/logo.png')}/>
                    <Text style={styles.textLog}>Login</Text>
                    <View style={styles.formLog}>
                        <Text style={styles.label}>Username / Email</Text>
                        <TextInput style={styles.input}/>
                    </View>
                    <View style={styles.formLog}>
                        <Text style={styles.label}>Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true}/>
                    </View>
                    <View style={styles.login}>
                        <TouchableOpacity style={styles.btnMasuk}>
                            <Text style={styles.textBtn}>Masuk</Text>
                        </TouchableOpacity>
                        <Text style={styles.textAtau}>atau</Text>
                        <TouchableOpacity style={styles.btnDaftar}>
                            <Text style={styles.textBtn}>Daftar ?</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    containerView: {
        backgroundColor: '#fff',
        marginTop: 63,
        alignItems: 'center',
        flex: 1
    },
    textLog: {
        marginVertical: 20,
        fontSize: 24,
        textAlign: 'center',
        color: '#003366'
    },
    formLog: {
        width: 294,
        marginHorizontal: 20, 
        marginVertical: 5,
        alignContent: 'center'
    },
    label: {
        color: '#003366'
    },
    input: {
        borderWidth: 1,
        height: 42,
        borderColor: '#003366'
    },
    login: {
        marginTop: 24,
    },
    btnMasuk: {
        width: 140,
        height: 40,
        borderRadius: 16,
        backgroundColor: '#3EC6FF',
        alignItems: 'center',
        marginBottom: 10
    },
    textBtn: {
        fontSize: 24,
        lineHeight: 37,
        color: '#fff',
        textAlign: "center",
    },
    textAtau: {
        fontSize: 24,
        textAlign: 'center',
        color: '#3EC6FF',
    },
    btnDaftar: {
        width: 140,
        height: 40,
        borderRadius: 16,
        backgroundColor: '#003366',
        alignItems: 'center',
        marginTop: 10
    }
});