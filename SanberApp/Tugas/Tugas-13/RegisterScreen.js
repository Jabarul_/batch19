import React from 'react';
import { Platform, StyleSheet, Text,  View,  Image,  TextInput,  TouchableOpacity,  Button,  KeyboardAvoidingView, ScrollView } from 'react-native';

export default class App extends React.Component{
    render() {
        return(
            <ScrollView>
                <View style={styles.containerView}>
                    <Image source={require('./assets/logo.png')}/>
                    <Text style={styles.textReg}>Register</Text>
                    <View style={styles.formLog}>
                        <Text style={styles.label}>Username</Text>
                        <TextInput style={styles.input}/>
                    </View>
                    <View style={styles.formLog}>
                        <Text style={styles.label}>Email</Text>
                        <TextInput style={styles.input} textContentType={'emailAddress'}/>
                    </View>
                    <View style={styles.formLog}>
                        <Text style={styles.label}>Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true}/>
                    </View>
                    <View style={styles.formLog}>
                        <Text style={styles.label}>Ulangi Password</Text>
                        <TextInput style={styles.input} secureTextEntry={true}/>
                    </View>
                    <View style={styles.regis}>
                        <TouchableOpacity style={styles.btnDaftar}>
                            <Text style={styles.textBtn}>Daftar</Text>
                        </TouchableOpacity>
                        <Text style={styles.textAtau}>atau</Text>
                        <TouchableOpacity style={styles.btnMasuk}>
                            <Text style={styles.textBtn}>Masuk ?</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    containerView: {
        flex: 1,
        backgroundColor: '#fff',
        marginTop: 63,
        alignItems: 'center'
    },
    textReg: {
        marginVertical: 20,
        fontSize: 24,
        textAlign: 'center',
        color: '#003366'
    },
    formLog: {
        width: 294,
        marginHorizontal: 20, 
        marginVertical: 5,
        alignContent: 'center'
    },
    label: {
        color: '#003366'
    },
    input: {
        borderWidth: 1,
        height: 42,
        borderColor: '#003366'
    },
    regis: {
        marginTop: 20
    },
    btnDaftar: {
        width: 140,
        height: 40,
        borderRadius: 16,
        backgroundColor: '#003366',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 10
    },
    textAtau: {
        fontSize: 24,
        textAlign: 'center',
        color: '#3EC6FF',
    },
    btnMasuk: {
        width: 140,
        height: 40,
        borderRadius: 16,
        backgroundColor: '#3EC6FF',
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 100
    },
    textBtn: {
        fontSize: 24,
        lineHeight: 37,
        color: '#fff',
        textAlign: "center",
    },
});