import React from 'react'
import {
    Platform,
    StyleSheet,
    Text, 
    View, 
    Image, 
    TextInput, 
    TouchableOpacity, 
    Button, 
    KeyboardAvoidingView,
    ScrollView 
} from 'react-native'

const AboutScreen = () => {
    return (
        <View style={styles.about}>
            <Text style={styles.txtAbout}>About</Text>
        </View>
    )
}
    
export default AboutScreen

const styles = StyleSheet.create({
    about: {
        flex: 1,
        justifyContent: 'center'
    },
    txtAbout: {
        fontSize: 16,
        textAlign: 'center'
    }
})