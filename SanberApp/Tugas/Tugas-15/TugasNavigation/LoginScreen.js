import React from 'react'
import {
    Platform,
    StyleSheet,
    Text, 
    View, 
    Image, 
    TextInput, 
    TouchableOpacity, 
    Button, 
    KeyboardAvoidingView,
    ScrollView 
} from 'react-native'

const LoginScreen = ({navigation}) => {
    return (
        <View style={styles.login}>
            <Text style={styles.loginText}>Login Screen</Text>
            <TouchableOpacity style={styles.btnLogin} onPress={ () => navigation.navigate('DrawerStackScreen') }>
                <Text style={styles.txtMenuju}>Menuju Skill Screen</Text>
            </TouchableOpacity>
        </View>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    login: {
        flex: 1,
        justifyContent: 'center'
    },
    loginText: {
        fontSize: 16,
        textAlign: 'center'
    },
    btnLogin: {
        backgroundColor: 'yellow',
        width: 110,
        padding: 10,
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    txtMenuju: {
        fontSize: 16
    }
})