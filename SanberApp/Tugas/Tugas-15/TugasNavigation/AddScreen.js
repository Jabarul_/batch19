import React from 'react'
import {
    Platform,
    StyleSheet,
    Text, 
    View, 
    Image, 
    TextInput, 
    TouchableOpacity, 
    Button, 
    KeyboardAvoidingView,
    ScrollView 
} from 'react-native'

const AddScreen = () => {
    return (
        <View style={styles.add}>
            <Text style={styles.txtTambah}>Halaman Tambah</Text>
        </View>
    )
}
    
export default AddScreen

const styles = StyleSheet.create({
    add: {
        flex: 1,
        justifyContent: 'center'
    },
    txtTambah: {
        fontSize: 16,
        textAlign: 'center'
    }
})