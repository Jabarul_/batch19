import React from 'react'
import {
    Platform,
    StyleSheet,
    Text, 
    View, 
    Image, 
    TextInput, 
    TouchableOpacity, 
    Button, 
    KeyboardAvoidingView,
    ScrollView 
} from 'react-native'

const SkillScreen = () => {
    return (
        <View style={styles.skill}>
            <Text style={styles.txtSkill}>Skill</Text>
        </View>
    )
}
    
export default SkillScreen

const styles = StyleSheet.create({
    skill: {
        flex: 1,
        justifyContent: 'center'
    },
    txtSkill: {
        fontSize: 16,
        textAlign: 'center'
    }
})