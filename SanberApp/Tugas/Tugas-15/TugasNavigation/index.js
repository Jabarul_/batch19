import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';
import SkillScreen from './SkillScreen';
import AddScreen from './AddScreen';
import ProjectScreen from './ProjectScreen';

const RootStack = createStackNavigator();
const TabsStack = createBottomTabNavigator();
const DrawerStack = createDrawerNavigator();

const TabsStackScreen = () => (
    <TabsStack.Navigator>
        <TabsStack.Screen name='SkillScreen' component={SkillScreen}
            options={{
                title: 'Test1'
            }} />
        <TabsStack.Screen name='ProjectScreen' component={ProjectScreen}
            options={{
                title: 'Proyek'
            }} />
        <TabsStack.Screen name='AddScreen' component={AddScreen}
            options={{
                title: 'Plus'
            }}
        />
    </TabsStack.Navigator>
);

const DrawerStackScreen = () => (
    <DrawerStack.Navigator >
        <DrawerStack.Screen name='TabsStackScreen' component={TabsStackScreen}
            options={{
                title: 'Home'
            }} 
        />
        <DrawerStack.Screen name='AboutScreen' component={AboutScreen}
            options={{
                title: 'About'
            }}
        />
    </DrawerStack.Navigator>
);

const RootStackScreen = () => (
    <RootStack.Navigator>
        <RootStack.Screen name='LoginScreen' component={LoginScreen}
            options={{
                title: 'Login'
            }}
        />
        <RootStack.Screen name='DrawerStackScreen' component={DrawerStackScreen}
            options={{
                title: 'Skill'
            }}
        />
    </RootStack.Navigator>
);

export default () => (
    <NavigationContainer>
        <RootStackScreen />
    </NavigationContainer>
);
