import React from 'react'
import {
    Platform,
    StyleSheet,
    Text, 
    View, 
    Image, 
    TextInput, 
    TouchableOpacity, 
    Button, 
    KeyboardAvoidingView,
    ScrollView 
} from 'react-native'

const ProjectScreen = () => {
    return (
        <View style={styles.project}>
            <Text style={styles.txtProyek}>Halaman Proyek</Text>
        </View>
    )
}
    
export default ProjectScreen

const styles = StyleSheet.create({
    project: {
        flex: 1,
        justifyContent: 'center'
    },
    txtProyek: {
        fontSize: 16,
        textAlign: 'center'
    }
})