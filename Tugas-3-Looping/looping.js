// No. 1 - Looping While
console.log("No. 1 - While Loop");
console.log("LOOPING PERTAMA");

var jumlah1 = 0;
var deret1 = 2;

while(jumlah1 < 20){
    jumlah1 =+ deret1;
    deret1 += 2;
    console.log(`${jumlah1} - I Love Coding`);
}

console.log("LOOPING KEDUA");

var jumlah2 = 22;

while(jumlah2 > 2 ){
    jumlah2 -= 2;
    console.log(`${jumlah2} - I will become a mobile developer`);
}

// No. 2 - Foor Loop
console.log("<============================>");
console.log("No. 2 - For Loop");

for(var angka = 1; angka <= 20; angka++){
    if(angka % 2 == 0){
        console.log(`${angka} - Berkualitas`);
    }else if(angka % 3 == 0){
        console.log(`${angka} - I Love Coding`);
    }else{
        console.log(`${angka} - Santai`);
    }
}

// No. 3 - Persegi Panjang
console.log("<============================>");
console.log("No. 3 - Persegi Panjang");

for(var i = 0; i <= 3; i++){
    var tangga = "#".repeat(8);
    console.log(tangga);
}

// No. 4 - Segitiga
console.log("<============================>");
console.log("No. 4 - Tangga");

var z= "";
for(var i = 0; i < 7; i++){
    z += "#"; 
    console.log(z);
}

// No. 5 - Papan Catur
console.log("<============================>");
console.log("No. 5 - Pepan Catur");

for(var i = 0; i < 8; i++){
    if(i % 2 == 0){
        console.log("# # # # ");
    }else{
        console.log(" # # # #");
    }
}