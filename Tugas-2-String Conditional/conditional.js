// if-else
var nama = "John";
var peran = "";

if(nama == "John" && peran == ""){
    console.log("Halo " + nama + ", pilih peranmu untuk memulai game!");
}else{
    if(nama == "Jane" && peran == "Penyihir"){
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Halo " + peran + " " + nama + ", kamu dapat melihat siapa yang menjadi Werewolf!");
    }else if(nama == "Jenita" && peran == "Guard"){
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Halo " + peran + " " + nama + ", kamu akan membantu melindungi temanmu dari serangan Werewolf.");  
    }else if(nama == "Junaedi" && peran == "Werewolf"){
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Halo " + peran + " " + nama + ", kamu akan memakan mangsa setiap malam!");
    }else{
        console.log("Nama harus diisi!");
    }
}

console.log("<=========================>");

// Switch Case
var tanggal = 1;
var bulan = 5;
var tahun = 1945;

switch(bulan){
    case 1:
        console.log(tanggal + " Januari " + tahun);
        break;
    case 2:
        console.log(tanggal + " Februari " + tahun);
        break;
    case 3:
        console.log(tanggal + " Maret " + tahun);
        break;
    case 4:
        console.log(tanggal + " April " + tahun);
        break;
    case 5:
        console.log(tanggal + " Mei " + tahun);
        break;
    case 6:
        console.log(tanggal + " Juni " + tahun);
        break;
    case 7:
        console.log(tanggal + " Juli " + tahun);
        break;
    case 8:
        console.log(tanggal + " Agustus " + tahun);
        break;
    case 9:
        console.log(tanggal + " September " + tahun);
        break;
    case 10:
        console.log(tanggal + " Oktober " + tahun);
        break;
    case 11:
        console.log(tanggal + " November " + tahun);
        break;
    case 12:
        console.log(tanggal + " Desember " + tahun);
        break;
}