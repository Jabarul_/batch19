// No. 1
function arrayToObject(arr){
    if(arr.length <= 0){
        console.log(" ");
    }

    for(var i = 0; i < arr.length; i++){
        var obj = {};
        var now = new Date();
        var thisYear = now.getFullYear();
        var selisih;
        var tahun = arr[i][3];

        if(tahun && thisYear - tahun > 0){
            selisih = thisYear - tahun;
        }else{
            selisih = "Invalid birth year";
        }

        obj.firstName = arr[i][0];
        obj.lastName = arr[i][1];
        obj.gender = arr[i][2];
        obj.age = selisih;

        var penomoran = `${obj.firstName} ${obj.lastName}:`;
        console.log(i+1+".", penomoran);
        console.log(obj);
    }
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people);

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2);

arrayToObject([]);

// No. 2
function shoppingTime(memberId, money){
    if(!memberId){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }else if(money < 50000){
        return "Mohon maaf, uang tidak cukup";
    }else{
        var obj = {};
        var array = [];
        var sepatu = "Sepatu Stacattu";
        var bZoro = "Baju Zoro";
        var bHN = "Baju H&N";
        var sweater = "Sweater Uniklooh";
        var casing = "Casing Handphone";
        var saldo = money;

        for(var i = 0; saldo >= 50000; i++){
            if(saldo >= 1500000){
                array.push(sepatu);
                saldo -= 1500000;
            }else if(saldo >= 500000){
                array.push(bZoro);
                saldo -= 500000;
            }else if(saldo >= 250000){
                array.push(bHN);
                saldo -= 250000;
            }else if(saldo >= 175000){
                array.push(sweater);
                saldo -= 175000;
            }else if(saldo >= 50000){
                    array.push(casing);
                    saldo -= 50000;
            }
        }

        obj.memberId = memberId;
        obj.money = money;
        obj.listPurchased = array;
        obj.changeMoney = saldo;
        return obj;
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());

// No. 3
function naikAngkot(arrPenumpang){
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var array = [];

    if (arrPenumpang.length <= 0){
        return [];
    }

    for(var i = 0; i< arrPenumpang.length; i++){
        var temp    = { };
        var dari    = arrPenumpang[i][1];
        var sampai  = arrPenumpang[i][2];
        var tempDari;
        var tempSampai;
    for (var j = 0; j<rute.length; j++){
        if(rute[j] == dari){
            tempDari = j;
        }else if (rute[j] == sampai){
            tempSampai = j;
        }
    }

    var bayar = (tempSampai- tempDari) * 2000
    
    temp.penumpang  = arrPenumpang[i][0];
    temp.naikDari   = dari;
    temp.tujuan     = sampai;
    temp.bayar      = bayar;

    array.push(temp);
    }
    return array;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));