var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let i = 0;
let waktu = 10000;

async function mulai(){
    for(i; i < books.length; i++){
        await readBooksPromise(waktu, books[i])
        .then(function(ada){
            return ada;
        })
        .catch(function(habis){
            return habis;
        })
    }
}

mulai();