// No. 1
console.log("No. 1");

function range(startNum, finishNum){
    var z =[];
    if(startNum > finishNum){
        for(var i = startNum; i >= finishNum; i--){
            z.push(i);
        }
    }else if(startNum < finishNum){
        for(var i = startNum; i <= finishNum; i++){
            z.push(i);
        }
    }else{
        z.push(-1);
    }
    return z;
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());

console.log("<------------------------------->");

// No. 2
console.log("No. 2");

function rangeWithStep(startNum, finishNum, step){
    var z = [];
    if(startNum > finishNum){
        for(var i = startNum; i >= finishNum; i -= step){
            z.push(i);
        }
    }else{
        for(var i = startNum; i <= finishNum; i += step){
            z.push(i);
        }
    }
    return z;
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log("<------------------------------->");

// No. 3
console.log("No. 3");

function sum(awal, akhir, step){
    var z = [];
    var jarak;
    if(!step){
        jarak = 1;
    }else{
        jarak = step;
    }

    if(awal > akhir){
        for(var i = 0; awal >= akhir; i--){
            z.push(awal);
            awal -= jarak;
        }
    }else if(awal < akhir){
        for(var i = 0; awal <= akhir; i++){
            z.push(awal);
            awal += jarak;
        }
    }else if(!awal && !akhir && !step){
        return 0;
    }else if(awal){
        return awal;
    }

    var total = 0;
    for(var i = 0; i < z.length; i++){
        total += z[i];
    }
    return total;
}

console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

console.log("<------------------------------->");

// No. 4
console.log("No. 4");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandling(){
    for(var i = 0; i < 4; i++){
        for(var j = 0; j < 5; j++){
            if(j == 0){
                console.log(`Nomor ID: ${input[i][j]}`);
            }else if(j == 1){
                console.log(`Nama Lengkap: ${input[i][j]}`);
            }else if(j == 2){
                var ttl = `TTL: ${input[i][j]} `;
            }else if(j == 3){
                console.log(ttl + input[i][j]);
            }else{
                console.log(`Hobi: ${input[i][j]} \n`);
            }
        }
    }
}

dataHandling();

console.log("<------------------------------->");

// No. 5
console.log("No. 5");

function balikKata(kata){
    var kosong = "";
    for(var i = kata.length -1; i >= 0; i--){
        kosong += kata[i];
    }
    return kosong;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

console.log("<------------------------------->");

// No. 6
console.log("No. 6");

function dataHandling2(input){
    // Splice
    input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    input.splice(4, 1, "Pria", "SMA International Metro");
    console.log(input);

    // Split & Switch Case
    var tbt = input[3].split("/");
    switch(tbt[1]){
        case "01":
            console.log("Januari");
            break;
        case "02":
            console.log("Februari");
            break;
        case "03":
            console.log("Maret");
            break;
        case "04":
            console.log("April");
            break;
        case "05":
            console.log("Mei");
            break;
        case "06":
            console.log("Juni");
            break;
        case "07":
            console.log("Juli");
            break;
        case "08":
            console.log("Agustus");
            break;
        case "09":
            console.log("September");
            break;
        case "10":
            console.log("Oktober");
            break;
        case "11":
            console.log("November");
            break;
        case "12":
            console.log("Desember");
            break;
    }

    // Split & sorting descending
    var bln = input[3].split("/");
    var desc = bln.sort(function(value1, value2){ return value2 - value1 });
    console.log(desc);

    // join
    var tgl = input[3].split("/");
    var gabung = tgl.join("-");
    console.log(gabung);

    // slice
    var nm = input[1];
    var potong = nm.slice(0, 15);
    console.log(potong);
}

var array = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];

dataHandling2(array);